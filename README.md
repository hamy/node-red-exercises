# Node-RED Exercises

![icon](icons/node-red-icon-256.png)



## Node-RED Links

* https://nodered.org/ - The Node-RED start page.
* https://flows.nodered.org/ - The Node-RED flow and module library.
* http://noderedguide.com/ - The Node-RED programming guide.

## Frequently Used Modules


| Name | Description | Installation |
| ---- | ----------- | ------------ |
| [Base64](https://www.npmjs.com/package/node-red-node-base64) | Base-64 module | sudo npm install -g node-red-node-base64 |
| [Dashboard](https://flows.nodered.org/node/node-red-dashboard) | Dashboard GUI nodes for Node-Red | sudo npm install -g node-red-dashboard |
| [Date/Time](https://flows.nodered.org/node/node-red-contrib-simpletime) | Simple date and time provider | sudo npm install -g node-red-contrib-simpletime added docs |
| [Ping](https://www.npmjs.com/package/node-red-node-ping) | Ping module | sudo npm install -g node-red-node-ping |
 

## Node-RED Demos

* [A Simple Barcode Publishing and Submission Demo](Barcode-Pub-Sub/)
* [Ping Loss Indicator Demo](Ping-Loss-Indicator/)
