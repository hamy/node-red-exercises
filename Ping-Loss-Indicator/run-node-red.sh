#!/bin/bash
node-red --settings ./settings.js \
         --userDir . \
	 --verbose \
	 flow.json \
         > /tmp/Ping-Loss-Indicator.stdout \
         2> /tmp/Ping-Loss-Indicator.stderr &
 
