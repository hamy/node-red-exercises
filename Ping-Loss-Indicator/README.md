# Ping Loss Indicator Demo                         

This demo periodically checks the connection to a host which is defined by
its DNS name or its IP address. Connection availability is indicated
by two short green flare-ups of a 
[ThingM Blink(1)](https://blink1.thingm.com/) smart LED. Otherwise,
a longer red-violet pulse indicates connection failures.

![flow-gui.png](images/flow-gui.png)

The ping round-trip time is also visualized in a dashboard:

![dashboard.png](images/dashboard.png)

## Flow

* [flow.json](flow.json) - The Node-RED flow definition (pretty-print version)
* [package.json](package.json) - Node.JS package declaration
* [settings.js](settings.js) - The settings for the Node-RED server
* [run-node-red.sh](run-node-red.sh) - A tiny script that runs Node-RED with
  the flow mentioned above.
* http://localhost:28002/ - The flow editor (only available if Node-RED runs on
  the local host)
* http://localhost:28002/ui - The dashboard (only available if Node-RED runs on
  the local host)



