# A Simple Barcode Publishing and Submission Demo

![flow-gui.png](images/flow-gui.png)

## Flow

* [flow.json](flow.json) - The Node-RED flow definition (pretty-print version)
* [package.json](package.json) - Node.JS package declaration
* [settings.js](settings.js) - The settings for the Node-RED server
* [run-node-red.sh](run-node-red.sh) - A tiny script that runs Node-RED with
  the flow mentioned above.
* http://localhost:28001/ - The flow editor (only available if Node-RED runs on
  the local host)
* [barcode2mosquitto](scripts/barcode2mosquitto) - A Unix shell scripts that captures
  barcodes from a camera and injects them via MQTT into the flow.



## Barcode Samples

### Code-39 Sample

![barcode-code-39](barcode-samples/barcode-code-39.png)

CODE-39:1159699

### EAN-13 Sample 
 
![barcode-ean-13](barcode-samples/barcode-ean-13.png)

EAN-13:4006544496107

### QR Code Sample

![barcode-qr-code](barcode-samples/barcode-qr-code.png)

QR-Code:http://www.oreilly.com/go/9781491938454-QR


